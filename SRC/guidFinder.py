import re

csproj_path = 'application.csproj'

with open(csproj_path, 'r') as file:
    csproj_content = file.read()
guid_pattern = r'<ProjectGuid>{(.*?)}<\/ProjectGuid>'
guids_found = re.findall(guid_pattern, csproj_content)

# convert GUIDs to uppercase versions
def upper_guid(match):
    return match.group(1).upper()

if guids_found:
    # Replace the GUIDs with uppercase versions
    patched_content = re.sub(guid_pattern, upper_guid, csproj_content)

    # Update the file
    with open(csproj_path, 'w') as file:
        file.write(patched_content)

    print("GUIDs have been patched to uppercase.")

else:
    raise FileNotFoundError("Exception: No GUID found in the csproj file")

    